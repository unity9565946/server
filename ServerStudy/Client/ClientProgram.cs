﻿using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Client;

public class ClientProgram
{
    public string id { get; private set; }

    private Socket _ConnectedServerSocket;


    private static async Task Main(string[] args)
    {
        ClientProgram client = new ClientProgram();

        // 서버에 연결합니다.
        await client.ConnectAsync();

        // 서버에 연결시킨 후, 프로그램을 시작시킵니다.
        await client.RunProgram();
    }

    private async Task RunProgram()
    {
        // 메인 페이지 표시
        await ShowMainPage();

        // 채팅 페이지 표시
        await ShowChattingPage();
    }

    public async Task ShowMainPage()
    {
        while (true)
        {
            // 메뉴 표시
            int select = ShowMenuPage();

            // 잘못된 입력 처리
            if (select != 1 && select != 2)
            {
                Console.Clear();
                continue;
            }

            // 로그인 페이지 출력
            bool isLogin = false;
            if (select == 1) isLogin = await ShowLoginPage();

            // 회원가입 페이지 출력
            else if (select == 2) await ShowRegiseterPage();

            if (isLogin) break;
        }
    }

    public async Task ShowChattingPage()
    {
        // 수신용 쓰레드 시작
        Thread receiveThread = new Thread(async () => await ReceiveChattingProcedure());
        receiveThread.Start();

        // 사용자 입력 받기
        while (true)
        {
            string chat = Console.ReadLine();

            // 사용자가 입력한 내용을 보냅니다.
            await Packet.SendAsync<ChatContext>(_ConnectedServerSocket, PacketType.SendStringRequest, new ChatContext(id, chat));
        }
    }

    private async Task ReceiveChattingProcedure()
    {
        while (true)
        {
            // 데이터를 수신합니다.
            PacketType packetType = await Packet.ReceiveAsyncPacketType(_ConnectedServerSocket);

            // 다른 유저 채팅 수신
            if (packetType == PacketType.ReceiveOtherUserChatting)
            {
                // 받은 데이터 표시
                ChatContext context = await Packet.ReceiveAsyncHeaderAndData<ChatContext>(_ConnectedServerSocket);

                Console.WriteLine(context.GetContext());
            }

        }
    }

    public async Task ConnectAsync()
    {
        _ConnectedServerSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        IPEndPoint endPoint = new IPEndPoint(IPAddress.Parse("192.168.3.68"), 20000);
        await _ConnectedServerSocket.ConnectAsync(endPoint);
    }

    /// <summary>
    /// 메뉴 페이지 표시
    /// </summary>
    /// <returns></returns>
    private int ShowMenuPage()
    {
        Console.WriteLine("1 ) 로그인");
        Console.WriteLine("2 ) 회원가입");

        return int.Parse(Console.ReadLine());

    }

    /// <summary>
    /// 로그인 페이지 출력
    /// </summary>
    private async Task<bool> ShowLoginPage()
    {
        while (true)
        {
            Console.WriteLine("로그인 해주세요");

            Console.WriteLine("1. 취소");
            Console.WriteLine("2. 로그인");
            int select = int.Parse(Console.ReadLine());
            select = select == 1 ? 1 : 2;
            if (select == 1) return false;
            

            Console.Write("아이디 입력 : ");
            string id = Console.ReadLine();

            Console.Write("비밀번호 입력 : ");
            string pw = Console.ReadLine();

            // 계정 정보를 이용하여 로그인을 시도합니다.
            await Packet.SendAsync(_ConnectedServerSocket, PacketType.LoginRequest, new AccountDataPacket(id, pw));

            // 로그인 결과 확인
            await Packet.ReceiveAsyncPacketType(_ConnectedServerSocket);
            SimpleResponsePacket loginResponce = await Packet.ReceiveAsyncHeaderAndData<SimpleResponsePacket>(_ConnectedServerSocket);

            // 로그인 성공
            if (loginResponce.isSucceeded)
            {
                // 로그인 한 id 기록
                this.id = id;

                Console.Clear();
                Console.WriteLine("로그인에 성공하였습니다,");
                await Task.Delay(2000);
                Console.Clear();
                return true;
            }
            else
            {
                Console.Clear();
                Console.WriteLine("로그인에 실패하였습니다.");
                Console.WriteLine("아이디와 비밀번호를 확인하세요!");
                await Task.Delay(2000);
                Console.Clear();
            }
        }
    }

    /// <summary>
    /// 회원가입 페이지 출력
    /// </summary>
    /// <returns></returns>
    private async Task ShowRegiseterPage()
    {
        while (true)
        {
            Console.WriteLine("회원가입 진행합니다.");
            Console.Write("아이디 입력 : ");
            string id = Console.ReadLine();

            Console.Write("비밀번호 입력 : ");
            string pw = Console.ReadLine();

            // 아이디 중복검사
            await Packet.SendAsync(
                _ConnectedServerSocket,
                PacketType.CheckNewIDRequest,
                new AccountDataPacket(id, pw));

            // 아이디 중복 검사 결과 확인
            await Packet.ReceiveAsyncPacketType(_ConnectedServerSocket);
            SimpleResponsePacket registerResponse = await Packet.ReceiveAsyncHeaderAndData<SimpleResponsePacket>(_ConnectedServerSocket);

            // 회원가입 성공
            if (registerResponse.isSucceeded)
            {
                Console.Clear();
                Console.WriteLine("회원가입이 완료되었습니다.");
                await Task.Delay(2000);
                Console.Clear();
                break;
            }
            // 회원가입 실패
            else
            {
                Console.Clear();
                Console.WriteLine("증복된 아이디입니다.");
                await Task.Delay(2000);
                Console.Clear();
            }
        }

    }
}

// 바이트 정렬 방식
/// - 컴퓨터에서 연속된 데이터들을 byte 단위로 순서대로 저장하는 것을 의미합니다.
/// - byte 가 저장되는 순서에 따라 빅엔디언, 리틀 엔디언 방식으로 나뉩니다.
/// 
/// - 네트워크를 통해 데이터를 보내는 경우 : HostToNetworkOrder
/// - 네트워크를 통해 받은 데이터를 가공하는 경우 : 
/// 
/// 빅 엔디언 (Big Endianess)
/// - 최하위 바이트부터 차례대로 저장되는 방식입니다.
/// 리틀 엔디언 (Little Endianess)
/// - 최상위 바이트부터 차례대로 저장되는 방식입니다.