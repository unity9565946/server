﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// 패킷 타입을 나타내는 열거 형식입니다.
/// </summary>
public enum PacketType : short
{
    IsDisconnected,                 // 클라이언트 / 서버 연결 종료됨

    CheckNewIDRequest,              // 아이디 중복 확인
    CheckNewIDResponse,             // 아이디 중복 확인 결과

    LoginRequest,                   // 로그인 요청
    LoginResponse,                  // 로그인 결과

    SendStringRequest,              // 문자열 보내기 요청
    ReceiveOtherUserChatting,       // 다른 유저의 채팅 수신
    
}

public static class Packet
{
    // 패킷 타입을 담는 바이트 배열 길이
    public const int PACKET_TYPE_BYTELENGTH = sizeof(PacketType);

    // 헤더를 담는 바이트 배열 길이
    public const int HEADER_BYTELENGTH = sizeof(short);

    /// <summary>
    /// T 형식의 데이터를 보냅니다
    /// </summary>
    /// <typeparam name="T">보낼 데이터의 형식</typeparam>
    /// <param name="socket">사용될 소켓 객체를 전달합니다.</param>
    /// <param name="packetType">패킷 타입을 전달합니다.</param>
    /// <param name="data">보낼 데이터를 전달합니다.</param>
    /// <returns></returns>
    public static async Task SendAsync<T>(Socket socket, PacketType packetType, T data) where T : struct
    {
        // 다음 순서로 바이트를 정렬하여 보냅니다
        // [PacketType] [Header] [.... Data ....]
        // 2바이트 : 패킷 타입
        // - 패킷 타입을 정의합니다.
        // 2바이트 : 헤더
        // - 이 다음에 오는 데이터의 크기를 정의합니다.
        // 나머지 바이트 : 데이터를 정의합니다.

        // 패킷 타입을 Byte 배열로 변환합니다.
        byte[] packetTypeToBytes = BitConverter.GetBytes(IPAddress.HostToNetworkOrder((short)packetType));

        // 보낼 데이터를 JSON 형태로 파싱합니다.
        string dataToJsonString = JsonConvert.SerializeObject(data);

        // Json 으로 파싱한 데이터를 Byte 배열로 변환합니다.
        byte[] dataToBytes = Encoding.UTF8.GetBytes(dataToJsonString);

        // 헤더에 담을 데이터를 Byte 배열로 변환합니다.
        byte[] headerBytes = BitConverter.GetBytes(IPAddress.HostToNetworkOrder((short)dataToBytes.Length));
        // - HostToNetworkOrder : 네트워크로 보내기 위한 바이트 정렬 방식을 따르도록 합니다.

        // 데이터를 보내기 위한 바이트 버퍼를 생성합니다.
        byte[] bufferToSend = new byte[PACKET_TYPE_BYTELENGTH + HEADER_BYTELENGTH + dataToBytes.Length];

        // 바이트 배열에 담을 데이터의 시작 인덱스를 나타내기 위한 변수
        int offset = 0;

        // 패킷 타입을 버터에 기록합니다.
        Array.Copy(packetTypeToBytes, 0, bufferToSend, offset, PACKET_TYPE_BYTELENGTH);
        offset += PACKET_TYPE_BYTELENGTH;

        // 헤더를 기록합니다.
        Array.Copy(headerBytes,0,bufferToSend,offset, HEADER_BYTELENGTH);
        offset += HEADER_BYTELENGTH;

        // 데이터를 기록합니다.
        Array.Copy(dataToBytes, 0, bufferToSend, offset, dataToBytes.Length);

        // 데이터를 보냅니다.
        await socket.SendAsync(bufferToSend, SocketFlags.None);
    }

    /// <summary>
    /// 패킷 타입을 수신합니다.
    /// </summary>
    /// <param name="socket">사용될 소켓 객체를 전달합니다.</param>
    /// <returns>수신된 패킷 타입이 반환됩니다.</returns>
    public static async Task<PacketType> ReceiveAsyncPacketType(Socket socket)
    {
        // 패킷 타입을 받기 위한 버퍼를 생성합니다.
        byte[] packetTypeBytes = new byte[PACKET_TYPE_BYTELENGTH];

        // 패킷 타입을 읽습니다.
        int readBytes = await socket.ReceiveAsync(packetTypeBytes, SocketFlags.None);

        // 0 바이트를 읽은 경우
        if (readBytes < 1)
        {
            // 연결 종료됨
            return PacketType.IsDisconnected;
        }
        // 0 바이트가 아닌, PACKET_TYPE_BYTELENGTH 보다 짧은 길이의 바이트를 읽은 경우
        // ( 아직 SocketTYpe 에 대한 내용을 덜 읽은 경우)
        else if (readBytes < PACKET_TYPE_BYTELENGTH)
        {
            // 더 읽도록 합니다.
            await socket.ReceiveAsync(
                new ArraySegment<byte>(packetTypeBytes,readBytes,PACKET_TYPE_BYTELENGTH - readBytes),
                SocketFlags.None);
            /// ArraySegment(T[] array, int offset, int count)
            ///  - 배열의 범위를 구분할 수 있도록 하기 위한 방식
            ///  T : 배열의 형식을 전달합니다.
            ///  array : 구분시킬 배열을 전달합니다.
            ///  offset : 배열 범위의 시작 위치를 나타냅니다.
            ///  count : 배열 범위 시작 위치부터 끝요소까지의 요소 개수를 나타냅니다.
            ///  배열의 특정 범위(offset + (offset + count)) 에 데이터를 기록하기 위하여 사용되었습니다.
        }

        // PACKET_TYPE_BYTELENGTH 만큼의 바이트를 읽은 경우 16Bit 크기의 정수 형식(short) 으로 변환하고
        // 이를 PacketType 형태로 캐스팅합니다.
        PacketType packetType = (PacketType)IPAddress.NetworkToHostOrder(BitConverter.ToInt16(packetTypeBytes));

        // 읽은 패킷 타입 반환
        return packetType;

    }

    /// <summary>
    /// 패킷 타입을 제외한 헤더와 데이터를 수신합니다.
    /// </summary>
    /// <typeparam name="T">받을 데이터 형식을 전달합니다.</typeparam>
    /// <param name="socket">사용될 소켓 객체를 전달합니다.</param>
    /// <returns>수신된 데이터를 반환합니다.</returns>
    public static async Task<T> ReceiveAsyncHeaderAndData<T>(Socket socket) where T : struct
    {
        // 헤더를 받기 위한 버퍼를 생성합니다.
        byte[] headerByte = new byte[HEADER_BYTELENGTH];

        // 헤더를 읽습니다.
        int readBytes = 0;
        while (readBytes < HEADER_BYTELENGTH)
        {
            // 헤더를 모두 읽습니다.
            int currentReadBytes = await socket.ReceiveAsync(
                new ArraySegment<byte>(headerByte, readBytes, HEADER_BYTELENGTH - readBytes), SocketFlags.None);
            readBytes += currentReadBytes;
        }

        // 헤더에 기록된 데이터 버퍼의 길이를 읽습니다
        int databufferLength = IPAddress.NetworkToHostOrder(BitConverter.ToInt16(headerByte));
        /// NetworkToHostOrder : 네트워크를 통해 받은 데이터를 해당 기기에서 사용하는 정렬 방식으로 변환합니다.

        // 데이터를 받기 위한 버퍼를 생성합니다.
        byte[] dataBytes = new byte[databufferLength];
        readBytes = 0;

        while(readBytes < databufferLength)
        {
            // 데이터를 모두 읽습니다.
            int currentReadBytes = await socket.ReceiveAsync(
                new ArraySegment<byte>(dataBytes, readBytes, databufferLength - readBytes), SocketFlags.None);
            readBytes += currentReadBytes;
        }

        // 데이터 바이트를 Json 문자열로 변환합니다.
        string bytesToJsonString = Encoding.UTF8.GetString(dataBytes);

        // Json 형식 문자열을 T 형식으로 변환합니다.
        T data = JsonConvert.DeserializeObject<T>(bytesToJsonString);

        // 읽은 데이터를 반환합니다.
        return data;

    }
}
