﻿
public struct ChatContext
{
    public string id;

    public string context;

    public ChatContext(string id, string context)
    {
        this.id = id;
        this.context = context;
    }

    public string GetContext()
    {
        return $"{id} : {context}";
    }

}

