﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public struct AccountDataPacket
{
    public string id;

    public string pw;

    public AccountDataPacket(string id, string pw)
    {
        this.id = id;
        this.pw = pw;
    }

}