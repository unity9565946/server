﻿using MySql.Data.MySqlClient;
using System.Net;
using System.Net.Sockets;

namespace Server;

// 네트워크
// 서로 다른 위치에 있는 기기를 여러 통신매체를 이용하여 연결시켜 놓은 것을 의미합니다.

// Socket
// 물리적으로 잘 연결되어있는 네트워크 상에서 데이터 송수신에 사용할 수 있는 소프트웨어적 장치

public class ServerProgram
{
    // 아이피
    private const string ADDRESS = "192.168.3.28";

    // 포트
    private const int PORT = 20000;

    /// <summary>
    /// 서버 소켓을 나타냅니다.
    /// </summary>
    private Socket _ServerSocket;

    /// <summary>
    /// 연결된 클라이언트 객체를 나타냅니다.
    /// </summary>
    private List<ConnectedClient> _Clients = new();

    /// <summary>
    /// MySql 연결 객체
    /// </summary>
    private MySqlConnection _MySqlConnection;

    /// <summary>
    /// 요청된 쿼리문
    /// </summary>
    private Queue<RequestQueryCommand> _RequestedQueryCommands = new();

    public class RequestQueryCommand
    {
        // 요청된 쿼리 명령어
        public string queryCommand;

        // NonQuery 방식이 아닌 경우 명령어 실행 이후 발생시킬 이벤트
        public System.Func<MySqlDataReader, Task> onExecuteReader;

        public bool isNonQuery => onExecuteReader == null;

        public RequestQueryCommand(string command, System.Func<MySqlDataReader, Task> onExecuteReader)
        {
            this.queryCommand = command;
            this.onExecuteReader = onExecuteReader;
        }
    }

    private static async Task Main(string[] args) => await new ServerProgram().WaitNewConnection();

    //Console.WriteLine("SERVER");
    //
    //
    //using (Socket serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp))
    //{
    //    // TCP (연결 지향형)
    //    // 데이터의 순서와 도착을 보장합니다. (신뢰성을 보장합니다.)
    //    // 1 : 1 연결을 지원합니다.
    //    // 게임이나 채팅프로그램에서 주로 사용되는 방식
    //
    //    // UDP (비 연결 지향형)
    //    // 데이터의 순서와 도착을 보장하지 않습니다. (신뢰성을 보장받지 못함)
    //    // TCP 보다 빠릅니다.
    //    // 1 : N 연결이 가능합니다.
    //    // 보통 라이브 스트리밍에서 사용됩니다.
    //
    //    //IPEndPoint endPoint = new IPEndPoint("192.168.3.28")
    //    IPEndPoint endPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 20000);
    //
    //    serverSocket.Bind(endPoint);
    //    serverSocket.Listen(20);
    //
    //    // 접속 요청된 클라이언트의 접속 허용
    //    using (Socket clientSocket = serverSocket.Accept())
    //    {
    //        // 접속된 클라이언트 IP 출력
    //        Console.WriteLine(clientSocket.RemoteEndPoint);
    //    }
    //}


    public ServerProgram()
    {
        IPEndPoint endPoint = new(IPAddress.Parse(ADDRESS), PORT);

        // 소켓 객체 생성
        _ServerSocket = new(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

        _ServerSocket.Bind(endPoint);

        _ServerSocket.Listen(20);

    }

    private async Task WaitNewConnection()
    {
        // MySql 접속
        using (_MySqlConnection = new($"Server = 121.141.17.150;Database=dbSample;Uid=BCLASS;Pwd=root;"))
        {
            // OPEN
            _MySqlConnection.Open();

            // 쿼리 명령어 프로시저 실행
            Thread queryCommandProcedure = new Thread(QueryCommandProcedure);
            queryCommandProcedure.Start();

            while (true)
            {
                // 연결 요청된 새로운 클라이언트의 연결 요청을 수락합니다.
                Socket connectClientSocket = await _ServerSocket.AcceptAsync();

                // 클라이언트 연결 요청을 수락하기 위해 쓰레드 풀에 등록합니다.
                ThreadPool.QueueUserWorkItem(OnClientConnected, connectClientSocket);

                // 연결된 클라이언트들을 OnClientConnected 메서드에서 처리됩니다.

            }

            queryCommandProcedure.Abort();
        }
    }

    /// <summary>
    /// 연결된 클라이언트들을 처리합 니다.
    /// </summary>
    /// <param name="sender">연결된 클라이언트 소켓 객체가 전달됩니다.</param>
    private void OnClientConnected(object sender)
    {
        // 연결 수락된 클라이언트 소켓 객체를 얻습니다.
        Socket clientSocket = sender as Socket;

        // 연결된 클라이언트 객체를 생성합니다.
        ConnectedClient newConnection = new(clientSocket);

        // 연결 해제 이벤트 등록
        newConnection.onDisconnected += OnClientDisconnected;

        // 쿼리문 실행 함수 등록
        newConnection.requestQueryCmd += RequestQueryCmd;

        // 채팅 이벤트 함수 등록
        newConnection.onSendString += OnClientChatRequested;

        // 연결된 클라이언트 객체를 리스트에 보관합니다.
        _Clients.Add(newConnection);
    }


    /// <summary>
    /// 하나의 클라이언트가 연결 해제 되었을 때 호출되는 콜백
    /// </summary>
    /// <param name="disconnectedClient"></param>
    private void OnClientDisconnected(ConnectedClient disconnectedClient) 
    {
        // 콘솔에 표시
        Console.WriteLine($"클라이언트 연결 해제됨! [{disconnectedClient.clientSocket.RemoteEndPoint}]");

        // 연결 해제
        disconnectedClient.clientSocket.Shutdown(SocketShutdown.Send);

        // 연결된 클라이언트 목록에서 제거합니다.
        _Clients.Remove(disconnectedClient);
    }

    /// <summary>
    /// 특정 클라이언트가 채팅을 보낸 경우 호출되는 메서드
    /// </summary>
    /// <param name="sender">보낸 클라이언트가 전달됩니다.</param>
    /// <param name="context">보낸 내용이 전달됩니다.</param>
    private async Task OnClientChatRequested(ConnectedClient sender, ChatContext context)
    {
        foreach(ConnectedClient client in _Clients)
        {
            // 채팅을 보낸 클라이언트는 제외합니다.
            if (client == sender) continue;

            // 다른 클라이언트에 데이터를 모두 보냅니다.
            await Packet.SendAsync<ChatContext>(client.clientSocket, PacketType.ReceiveOtherUserChatting, context);
        }
    }

    private async void QueryCommandProcedure()
    {
        while (true) 
        {
            // 처리할 요청이 존재하지 않는 경우 하단 구문을 실행하지 않습니다.
            if (_RequestedQueryCommands.Count == 0) continue;

            // 요청을 얻습니다.
            RequestQueryCommand requestQuery = _RequestedQueryCommands.Dequeue();

            // 요청된 명령을 생성합니다.
            MySqlCommand cmd = new(requestQuery.queryCommand, _MySqlConnection);

            if(requestQuery.isNonQuery)
            {
                cmd.ExecuteNonQuery();
            }
            else
            {
                MySqlDataReader table = cmd.ExecuteReader();
                await requestQuery.onExecuteReader(table);
                await table.CloseAsync();
            }

        }
    }

    /// <summary>
    /// 쿼리 요청
    /// </summary>
    /// <param name="queryCommand">명령어를 전달합니다.</param>
    /// <param name="onExecuteReader">NonQuery 방식이 아닌 경우 명령어 실행 이후 발생시킬 이벤트</param>
    private void RequestQueryCmd(
        string queryCommand, 
        System.Func<MySqlDataReader, Task> onExecuteReader = null)
    {
        // 다른 쓰레드에서 _RequestedQueryCommands 에 대한 동시 접근을 제한합니다.
        lock (_RequestedQueryCommands)
        {
            _RequestedQueryCommands.Enqueue(new(queryCommand, onExecuteReader));
        }
    }

}