﻿using MySql.Data.MySqlClient;
using System.Net.Sockets;

namespace Server;

public class ConnectedClient
{
    /// <summary>
    /// 서버와 연결된 클라이언트 소켓 객체를 나타냅니다.
    /// </summary>
    public Socket clientSocket { get; private set; }

    /// <summary>
    /// 로그인됨 상태를 나타냅니다.
    /// </summary>
    public bool isLogin { get; private set; }

    public event System.Action<string, System.Func<MySqlDataReader, Task>> requestQueryCmd;

    public event System.Action<ConnectedClient> onDisconnected;

    public event System.Func<ConnectedClient, ChatContext, Task> onSendString;

    public ConnectedClient(Socket clientSocket)
    {
        this.clientSocket = clientSocket;

        Console.WriteLine($"클라이언트에 연결됨! [{this.clientSocket.RemoteEndPoint}]");

        // 클라이언트 데이터 수신 시작
        new Thread(DoListen).Start();
    }

    /// <summary>
    /// 루프를 돌며 접속된 클라이언트들의 데이터를 받습니다.
    /// </summary>
    public async void DoListen()
    {
        try
        {
            while (true)
            {
                // 패킷을 읽습니다.
                PacketType packetType = await Packet.ReceiveAsyncPacketType(clientSocket);

                if (packetType == PacketType.IsDisconnected)
                {
                    // 클라이언트 연결 해제
                    onDisconnected?.Invoke(this);
                }

                // 회원가입 요청
                else if (packetType == PacketType.CheckNewIDRequest)
                {
                    // 요청을 받습니다.
                    AccountDataPacket request = await Packet.ReceiveAsyncHeaderAndData<AccountDataPacket>(clientSocket);

                    Console.WriteLine("아이디 중복 체크 요청됨");
                    Console.WriteLine($"ID : {request.id}");
                    Console.WriteLine($"PW : {request.pw}");

                    requestQueryCmd(
                        $"SELECT * FROM accoun_info", async (table) =>
                        {
                            // 중복 여부
                            bool isDuplicated = false;

                            while (table.Read())
                            {
                                // 이미 같은 ID 로 등록되어 있는 경우
                                if (table["id"].ToString() == request.id)
                                {
                                    isDuplicated = true;
                                    break;
                                }
                            }
                            // 중복되지 않은 경우
                            if (!isDuplicated)
                            {
                                // DB 에 요청된 아이디를 등록합니다.
                                requestQueryCmd($"INSERT INDO accounw_info (id, pw) VALUES (\'{request.id}\', \'{request.pw}\');", null);
                            }

                            // 결과를 전달합니다.
                            await Packet.SendAsync<SimpleResponsePacket>(clientSocket, PacketType.CheckNewIDResponse, new(!isDuplicated));

                        });

                }

                // 로그인 요청
                else if (packetType == PacketType.LoginRequest)
                {
                    // 데이터를 받습니다.
                    AccountDataPacket packet = await Packet.ReceiveAsyncHeaderAndData<AccountDataPacket>(clientSocket);

                    Console.WriteLine($"계정 로그인 요청됨. [id : {packet.id}] [pw : {packet.pw}]");

                    requestQueryCmd($"SELECT * FROM account_info", async (table) =>
                    {
                        while (table.Read())
                        {
                            // 요청된 id, pw 와 일하는 겨정이없다면
                            if (table["id"].ToString() == packet.id &&
                            table["pw"].ToString() == packet.pw)
                            {
                                // 로그인 상태롤 변경합니다.
                                isLogin = true;

                                break;
                            }

                        }
                        // 로그인 결과를 보냅니다
                        await Packet.SendAsync<SimpleResponsePacket>(clientSocket, PacketType.LoginResponse, new(isLogin));
                    });

                }

                // 채팅 데이터 수신
                else if(packetType == PacketType.SendStringRequest)
                {
                    // 보낸 데이터를 얻습니다.
                    ChatContext context = await Packet.ReceiveAsyncHeaderAndData<ChatContext>(clientSocket);

                    onSendString?.Invoke(this, context);
                }
            }
        }
        catch(System.ObjectDisposedException)
        {
            // 클라이언트 연결 해제
            onDisconnected?.Invoke(this);
        }
        catch (SocketException)
        {
            // 클라이언트 연결 해제
            onDisconnected?.Invoke(this);
        }
    }
}
